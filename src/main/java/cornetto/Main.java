package cornetto;

import com.crawljax.core.CrawljaxRunner;
import com.crawljax.core.configuration.BrowserConfiguration;
import com.crawljax.core.configuration.CrawlRules;
import com.crawljax.core.configuration.CrawljaxConfiguration;
import com.crawljax.core.configuration.CrawljaxConfiguration.CrawljaxConfigurationBuilder;
import com.crawljax.browser.EmbeddedBrowser.BrowserType;
import com.crawljax.core.configuration.ProxyConfiguration;
import com.crawljax.plugins.crawloverview.CrawlOverview;
import com.crawljax.browser.EmbeddedBrowser;

import cornetto.proxy.ZAPProxy;
import edu.umass.cs.benchlab.har.HarCreator;
import edu.umass.cs.benchlab.har.HarEntries;
import edu.umass.cs.benchlab.har.HarLog;
import edu.umass.cs.benchlab.har.tools.HarFileWriter;
import org.parosproxy.paros.Constant;
import org.parosproxy.paros.network.HttpMessage;
import org.zaproxy.zap.utils.HarUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;



public class Main
{
    static int proxyPort = 8089;
    static String proxyIp = "0.0.0.0";

    HarLog harLog = new HarLog(new HarCreator("cornetto", "1"));
    String target;

    public static void main( String[] args ) throws IOException {
        if (args.length != 1) {
            System.err.println("Usage: java cornetto.Main <targetURL>");
            System.exit(-1);
        }
        File out = new File("out");
        String target = args[0];
        Main m = new Main();
        m.target = target;
        m.harLog.setEntries(new HarEntries());
        ZAPProxy proxy = new ZAPProxy(8089, "config.xml");
        proxy.start(m);
        CrawljaxRunner crawljax = null;
        try {
            crawljax = new CrawljaxRunner(readConfig(target, out));
        } catch (Exception e) {
            System.err.println("Error configuring crawler");
            e.printStackTrace();
            System.exit(-1);
        }
        crawljax.call();
        System.out.println("Complete!");
        HarFileWriter w = new HarFileWriter();
        w.writeHarFile(m.harLog, new File("out.har"));
    }

    public synchronized void saveRequest(HttpMessage msg) {
        if (msg.getRequestHeader().getURI().toString().startsWith(target))
            harLog.getEntries().addEntry(HarUtils.createHarEntry(msg));
    }


    private static CrawljaxConfiguration readConfig(String urlValue, File outputDir) throws Exception {

        Properties props = new Properties();
        File propertiesFile = new File("crawler.properties");
        props.load(new FileInputStream(propertiesFile));

        CrawljaxConfigurationBuilder builder = CrawljaxConfiguration.builderFor(urlValue);
        builder.setOutputDirectory(outputDir);
        EmbeddedBrowser.BrowserType browser = BrowserType.valueOf(props.getProperty("browser"));

        int browsers = 1;
        builder.crawlRules().setFormFillMode(CrawlRules.FormFillMode.valueOf(props.getProperty("formFillMode")));
        builder.setBrowserConfig(new BrowserConfiguration(browser, browsers));
        builder.setProxyConfig(
                ProxyConfiguration.manualProxyOn(proxyIp, proxyPort));
        builder.setMaximumDepth(Integer.parseInt(props.getProperty("maxDepth")));
        builder.setMaximumStates(Integer.parseInt(props.getProperty("maxStates")));
        builder.crawlRules().crawlHiddenAnchors(true);
        configureTimers(builder,
                Integer.parseInt(props.getProperty("maxRunTime")),
                Integer.parseInt(props.getProperty("waitAfterEvent")),
                Integer.parseInt(props.getProperty("waitAfterReload")));
        builder.crawlRules().followExternalLinks(Boolean.valueOf(props.getProperty("followExternalLinks")));
        if (Boolean.valueOf(props.getProperty("crawlOverview")))
            builder.addPlugin(new CrawlOverview());
        builder.crawlRules().clickDefaultElements();
        return builder.build();
    }

    private static void configureTimers(CrawljaxConfigurationBuilder builder, int maxRunTime, int waitAfterEvent, int waitAfterReload) {
            builder.setMaximumRunTime(maxRunTime, TimeUnit.MINUTES);
            builder.crawlRules().waitAfterEvent(waitAfterEvent,
                    TimeUnit.MILLISECONDS);
            builder.crawlRules().waitAfterReloadUrl(waitAfterReload,
                    TimeUnit.MILLISECONDS);
    }




}

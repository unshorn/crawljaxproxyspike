package cornetto.proxy;


import cornetto.Main;

import org.apache.commons.httpclient.protocol.Protocol;
import org.apache.commons.httpclient.protocol.ProtocolSocketFactory;
import org.parosproxy.paros.core.proxy.ProxyListener;
import org.parosproxy.paros.core.proxy.ProxyParam;
import org.parosproxy.paros.core.proxy.ProxyServer;
import org.parosproxy.paros.network.ConnectionParam;
import org.parosproxy.paros.network.HttpMessage;
import org.parosproxy.paros.network.SSLConnector;
import org.zaproxy.zap.extension.dynssl.ExtensionDynSSL;
import org.zaproxy.zap.utils.ZapXmlConfiguration;

import java.io.File;

public class ZAPProxy  {

    private int port;
    private String configFile;
    static private ZAPProxy proxy;
    ProxyServer proxyServer;
    static Main m;

    public ZAPProxy(int port, String configFile) {
        this.port = port;
        this.configFile = configFile;
        proxy = this;
    }

    public static ZAPProxy getInstance() {
        return proxy;
    }
    public  void start(Main m)  {
        this.m = m;
        ExtensionDynSSL e = new ExtensionDynSSL();
        e.getParams().load(configFile);
        e.start();
        Protocol.registerProtocol(
                "https",
                new Protocol("https", (ProtocolSocketFactory) new SSLConnector(true), 443));
        ConnectionParam p = new ConnectionParam();
        p.load(new ZapXmlConfiguration());
        p.setSecurityProtocolsEnabled(new String[]{"SSLv3", "TLSv1", "TLSv1.1", "TLSv1.2", "TLSv1.3"});
        p.setUseProxyChain(false);
        proxyServer = new ProxyServer();
        proxyServer.setConnectionParam(p);
        proxyServer.getProxyParam().load(new ZapXmlConfiguration());
        ProxyParam proxyParam = proxyServer.getProxyParam();
        proxyParam.setAlwaysDecodeGzip(true);
        proxyParam.setBehindNat(false);
        proxyParam.setRemoveUnsupportedEncodings(true);
        /*try {
            e.writeRootFullCaCertificateToFile(Paths.get("/Users/computer/owasp_zap_root_ca.cer"));
        } catch (Exception ex) {

        }*/

        proxyServer.addProxyListener(new FeedbackProxyResponseListener());
        int j = proxyServer.startServer("127.0.0.1", 8089, false);
        System.err.println("Proxy strted!");

    }

    public void end() {
        proxyServer.stopServer();
    }

    private static class FeedbackProxyResponseListener implements ProxyListener {

        @Override
        public int getArrangeableListenerOrder() {
            return 0;
        }

        @Override
        public boolean onHttpRequestSend(HttpMessage httpMessage) {
            return true;
        }

        @Override
        public boolean onHttpResponseReceive(HttpMessage httpMessage) {
            m.saveRequest(httpMessage);
            System.err.println(httpMessage.getRequestHeader().getURI());
            return true;
        }
    }

}


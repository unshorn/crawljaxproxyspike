# crawljax+proxy

Use of [Crawljax](https://github.com/crawljax/crawljax) with [ZAP](https://github.com/zaproxy/zaproxy). Records crawl requests to HAR file.

## Usage
Crawler properties can be set in `crawler.properties`. e.g. `browser=FIREFOX_HEADLESS`.

```bash
	cd web
	python3 -m http.server &   # run http server for testing
	cd ..
	mvn clean compile
	mvn dependency:copy-dependencies
	java -cp target/classes:target/dependency/* cornetto.Main http://<host>:8000
```


Output HAR log is written to `out.har`. Crawljax overview (thumbnails of pages, state snapshots..etc) is written to `./out/`. This can be disabled with `crawlOverview=false` in `crawler.properties`.

**NOTE**: the target host should not be localhost or 127.0.0.1 as that can bypass the proxy. Use the external interface's IP address.
